<?php namespace Entopancore\Extendbackend\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration103 extends Migration
{
    public function up()
    {
        Schema::create('backend_lock', function ($table) {
            $table->increments('id');
            $table->string('url');
            $table->integer('backend_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('backend_lock');
    }
}