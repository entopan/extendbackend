<?php namespace Entopancore\Extendbackend\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration102 extends Migration
{
    public function up()
    {
        Schema::table('backend_users', function ($table) {
            $table->integer('nest_left')->unsigned()->nullabe();
            $table->integer('nest_right')->unsigned()->nullabe();
            $table->integer('nest_depth')->unsigned()->nullabe();
            $table->integer('parent_id')->unsigned()->nullabe();
            $table->string('code', 255)->nullable();
        });
    }

    public function down()
    {
        Schema::table('backend_users', function ($table) {
            $table->dropColumn('nest_left');
            $table->dropColumn('nest_right');
            $table->dropColumn('nest_depth');
            $table->dropColumn('parent_id');
            $table->dropColumn('code');
        });
    }
}