<?php namespace Entopancore\Extendbackend\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

class Dashboard extends Controller
{
    public $utilityClass;
    public $items;
    public $plusUtilities = [];

    public $requiredPermissions = [
        'entopancore.extendbackend.dashboard'
    ];


    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Entopancore.Extendbackend', 'dashboard', 'dashboard');

    }

    public function index()
    {
        $this->pageTitle = "Dashboard";
        $this->items = \BackendMenu::listMainMenuItems();
        if (\BackendAuth::getUser()->is_superuser) {
            array_push($this->plusUtilities, ["label" => "Settings", "url" => \Backend::url("system/settings")]);

        }
        if (\BackendAuth::isImpersonator() or \BackendAuth::getUser()->hasAccess('entopancore.extendbackend.impersonate')) {
            array_push($this->plusUtilities, ["label" => "Impersonator", "url" => \Backend::url("entopancore/extendbackend/impersonator")]);
        }

        if (\BackendAuth::getUser()->hasPermission("entopancore.extendbackend.media-manager") or \BackendAuth::getUser()->is_superuser) {
            array_push($this->plusUtilities, ["label" => "Media Manager", "url" => \Backend::url("backend/media")]);

        }

        if (\BackendAuth::getUser()->is_superuser) {
            array_push($this->plusUtilities, ["label" => "Builder", "url" => \Backend::url("rainlab/builder")]);

        }

    }

}