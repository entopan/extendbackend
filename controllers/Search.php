<?php namespace Entopancore\Extendbackend\Controllers;

use Backend\Classes\Controller;
use Backend\Models\User;
use BackendMenu;


class Search extends Controller
{
    public $requiredPermissions = [
        'entopancore.extendbackend.search'
    ];

    public $pageTitle = "Ricerca";

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $result = config("entopancore.extendbackend::search");
        foreach ($result as $k => $model) {
            $class = new $model["model"];
            foreach ($model["fields"] as $field) {
                $field = explode(".", $field);
                if (count($field) == 2) {
                    $class = $class->orWhereHas("$field[0]", function ($query) use ($field) {
                        $query->where($field[1], 'like', '%' . get("q") . '%');
                    });
                } else {
                    $class = $class->orWhere("$field[0]", 'like', '%' . get("q") . '%');
                }

            }
            if (isset($model["scope"])) {
                $class = $class->{$model["scope"]}();
            }

            $class = $class->take($model["take"])->get()->toArray();
            $result[$k]["result"] = $class;
        }
        $this->vars["result"] = $result;
    }

}