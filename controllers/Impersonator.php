<?php namespace Entopancore\Extendbackend\Controllers;

use Backend\Classes\Controller;
use Backend\Models\User;
use BackendMenu;


class Impersonator extends Controller
{
    public $requiredPermissions = [];

    public $pageTitle = "Impersonator";

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Entopancore.Extendbackend', 'impersonator', 'impersonator');

    }

    public function index()
    {
        if (\BackendAuth::isImpersonator()) {
            $this->vars["old"] = \BackendAuth::getImpersonator();
        } else {
            if (!\BackendAuth::getUser()->hasAccess('entopancore.extendbackend.impersonate')) {
                return \Response::make(\View::make('backend::access_denied'), 403);
            }

            if (config("entopancore.extendbackend::impersonator") === "all") {
                $exludeAdmins = [];
            } else if (config("entopancore.extendbackend::impersonator") === "default") {
                $exludeAdmins = User::whereHas("role", function ($query) {
                    $query->whereIn("code", config("entopancore.extendbackend::superadmin-code"));
                })->lists("id");
                foreach ($exludeAdmins as $id) {
                    if ($id === \BackendAuth::getUser()->id) {
                        $this->vars["owner"] = true;
                    }
                }
            }

            array_push($exludeAdmins, \BackendAuth::getUser()->id);
            $this->vars["users"] = User::with("role")->whereNotIn("id", $exludeAdmins)->where("is_superuser", "!=", 1)->get();
        }
        $this->vars["user"] = \BackendAuth::getUser();
    }

    public function onStopImpersonate()
    {
        \BackendAuth::stopImpersonate();
        return redirect(\Backend::url('backend'));
    }

    public function onUserSelect()
    {
        if ($user = User::find(post("user"))) {
            \BackendAuth::impersonate($user);
        }
        \Flash::success("Hai effettuato l'accesso come " . $user->full_name);
        return redirect()->to(\Backend::url("/"));

    }

    public function onSetSuperUser()
    {
        User::find(\BackendAuth::getUser()->id)->whereHas("role", function ($query) {
            $query->whereIn("code", config("entopancore.extendbackend::superadmin-code"));
        })->update(["is_superuser" => 1]);


        return back();
    }

    public function onUnsetSuperUser()
    {
        User::find(\BackendAuth::getUser()->id)->whereHas("role", function ($query) {
            $query->whereIn("code", config("entopancore.extendbackend::superadmin-code"));
        })->update(["is_superuser" => 0]);
        return back();
    }


    public function onSyncFacet()
    {
        try {
            exec("php artisan entopancore:facet-sync");
            \Flash::success("Sincronizzazione iniziata");
            return \Redirect::back();
        } catch (\Exeption $e) {
            \Flash::error("Errore");
            return \Redirect::back();
        }
    }

    public function onSyncFacetPlugins()
    {
        try {
            exec("php artisan entopancore:facet-sync --plugins=all");
            \Flash::success("Sincronizzazione iniziata");
            return \Redirect::back();
        } catch (\Exeption $e) {
            \Flash::error("Errore");
            return \Redirect::back();
        }
    }

    public function onImportFacet()
    {
        try {
            exec("php artisan entopancore:facet-import");
            \Flash::success("Importazione iniziata");
            return \Redirect::back();
        } catch (\Exeption $e) {
            \Flash::error("Errore");
            return \Redirect::back();
        }
    }

    public function onDetect()
    {
        try {
            exec("php artisan entopancore:facet-create-urls");
            \Flash::success("Creazione iniziata");
            return \Redirect::back();
        } catch (\Exeption $e) {
            \Flash::error("Errore");
            return \Redirect::back();
        }
    }

    public function onImportBox()
    {
        try {
            exec("php artisan entopancore:facet-box");
            \Flash::success("Importazione iniziata");
            return \Redirect::back();
        } catch (\Exeption $e) {
            \Flash::error("Errore");
            return \Redirect::back();
        }
    }




}