<?php

return [
    'v' =>
        [
            'company_fiscalcode.required' => 'Il codice fiscale è obbligatorio',
            'email.required' => 'Indirizzo email obbligatorio',
            'email.exists' => 'Email inesistente',
            'code.exists' => 'Codice non valido o scaduto',
            'address_name.required' => 'Il nome è obbligatorio',
            'address_name.regex' => 'Formato nome non valido',
            'address_address.required' => 'L\'indirizzo è obbligatorio',
            'address_number.required' => 'Il numero civico è obbligatorio',
            'address_city.required' => 'La città è obbligatoria',
            'address_city.regex' => 'Formato città non valido',
            'address_postalcode.required' => 'Il CAP è obbligatorio',
            'address_province.required' => 'La provincia è obbligatoria',
            'address_province.regex' => 'Formato provincia non valido',
            'address_phone.required' => 'Numero di telefono obbligatorio',
            'address_phone.numeric' => 'Formato telefono non valido',
            'name.required' => 'Inserisci il nome',
            'surname.required' => 'Inserisci il cognome',
            'password.between' => 'Digita una password con una lunghezza compresa tra :min e :max caratteri',
            'password.confirmed' => 'La conferma della password non corrisponde',
            'company_address.required' => 'Indirizzo obbligatorio',
            'company_vat.required' => 'Partita IVA obbligatoria',
            'company_name.required' => 'Nome/Ragione sociale obbligatorio',
            'company_address.regex' => 'Formato azienda non valido',
            'company_postalcode.required' => 'CAP obbligatorio',
            'company_city.required' => 'Città obbligatoria',
            'company_city.regex' => 'Formato città non valido',
        ],
    'm' =>
        [
            'update-billing' => 'Dati di fatturazione aggiornati con successo',
            'update-account' => 'Dati personali aggiornati con successo',
            'update-address' => 'Indirizzo di spedizione modificato con successo',
            'delete-address-false' => 'Non puoi rimuovere l\'indirizzo di spedizione predefinito',
            'delete-address-true' => 'Indirizzo di spedizione rimosso con successo',
            'restore-password' => 'Abbiamo inviato un messaggio al tuo indirizzo email'
        ],
    'e' =>
        [
            "password" => "Recupero password",
            "welcome" => ":name, benvenuto su :site",
            "new_user" => "Nuovo utente iscritto :name, :id, :email",
            "credentials" => ":name, ecco le tue credenziali d'accesso",
            "activation" => ":name, attiva il tuo profilo",
            "activation_admin" => "Nuovo utente registrato, attiva profilo",
        ]
];