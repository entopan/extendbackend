<?php return [
    'plugin' => [
        'name' => 'Estensione backend',
        'description' => '',
    ],
    'info' => 'Informazione',
    'welcome' => 'Benvenuto',
    'permissions' => [
        'see_intro_dashboard' => 'Vedi dentro la dashboard',
        'access_settings' => 'Accesso impostazioni'
    ],
    'wrong_password' => 'La password inserita non è corretta',
    'login_not_found' => 'Non è stato trovato un utente con questo username'
];

