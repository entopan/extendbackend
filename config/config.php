<?php

return
    [
        'superadmin-code' => ['owners'],
        'admin-code' => ['owners'],
        'impersonator' => 'default', // all -> exclude only self         // default -> exclude admin and self
        "dashboard" =>
            [
                "owners" => '\\entopancore\\extendbackend\Controllers\\Dashboard'
            ],
        'search' =>
            [

            ]
    ];