<?php


Route::get('/', ['uses' => 'Entopancore\Extendbackend\Http\Controllers\ExtendbackendController@redirect', 'as' => 'redirectBackend']);


Route::group(
    [
        'prefix' => 'api/v1/public/backend',
        'middleware' => [
            'api'
        ],
    ], function () {
    Route::post('login', ['uses' => 'Entopancore\Extendbackend\Http\Controllers\ExtendbackendController@login']);
    Route::get('code-login', ['uses' => 'Entopancore\Extendbackend\Http\Controllers\ExtendbackendController@codeLogin']);
    Route::post('restore', ['uses' => 'Entopancore\Extendbackend\Http\Controllers\ExtendbackendController@restore']);
    Route::post('reset', ['uses' => 'Entopancore\Extendbackend\Http\Controllers\ExtendbackendController@reset']);
    Route::get('logout', ['uses' => 'Entopancore\Extendbackend\Http\Controllers\ExtendbackendController@logout']);

});

Route::group(
    [
        'prefix' => 'api/v1/public/backend',
        'middleware' => [
            'api'
        ],
    ], function () {
    Route::get('check', ["uses" => 'Entopancore\Extendbackend\Http\Controllers\ExtendbackendController@check', 'as' => 'backendUserCheck']);
});
