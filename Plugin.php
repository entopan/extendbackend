<?php namespace Entopancore\Extendbackend;

use System\Classes\PluginBase;
use BackendAuth;

class Plugin extends PluginBase
{


    public function registerComponents()
    {
        return [];
    }


    public function register()
    {

    }

    public function boot()
    {
        \App::register('Entopancore\Extendbackend\Http\ExtendbackendServiceProvider');

        \Backend\Models\User::extend(function ($model) {
            $model->bindEvent('model.beforeCreate', function () use ($model) {
                $model->code = str_random(255);
            });
        });
    }

}
