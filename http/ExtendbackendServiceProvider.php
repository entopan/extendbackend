<?php namespace Entopancore\Extendbackend\Http;

use Illuminate\Support\ServiceProvider;

class ExtendbackendServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind("Entopancore\Extendbackend\Http\Repositories\ExtendbackendRepositoryInterface","Entopancore\Extendbackend\Http\Repositories\EloquentExtendbackendRepository");
    }

}
