<?php
/**
 * Created by PhpStorm.
 * User: entopanlab
 * Date: 05/06/17
 * Time: 9.23
 */

namespace Entopancore\Extendbackend\Http\Repositories;


interface ExtendbackendRepositoryInterface
{

    public function login($data);

    public function loginAccess($userId, $userIp);

    public function reset($code, $password);

    public function findUserByEmail($email);


}