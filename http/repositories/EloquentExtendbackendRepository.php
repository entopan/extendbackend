<?php namespace Entopancore\Extendbackend\Http\Repositories;

use Backend\Models\AccessLog;
use Backend\Models\User;
use Carbon\Carbon;


class EloquentExtendbackendRepository implements ExtendbackendRepositoryInterface
{

    public function login($data)
    {
        try {
            if ($user = User::where("login", "=", $data["login"])->first()) {

                if ($user->checkHashValue("password", base64_decode($data['password']))) {
                    $code = str_random(200);
                    $user->code = $code;
                    $user->is_activated = 1;
                    $user->last_login = Carbon::now();
                    $user->save();
                    $user = $user->toArray();
                    $link = route("backendCodeLogin", ["code" => $code]);
                    return ['status' => 200, "data" => ["link" => $link, "user" => $user, "token" => $code, 'id' => $user['id']], "message" => 'Benvenuto ' . $user['first_name']];
                } else {
                    return ['status' => 400, 'message' => 'Password errata'];
                }

            } else {
                return ['status' => 404, 'message' => 'Nessun utente trovato con questo username'];
            }
        } catch (\Exception $e) {
            info('error login:' . $e->getMessage());
            return ['status' => 500, 'message' => $e->getMessage()];
        }

    }

    public function loginAccess($userId, $userIp)
    {
        try {
            $accessLog = new AccessLog();
            $accessLog->user_id = $userId;
            $accessLog->ip_address = $userIp;
            $accessLog->save();
        } catch (\Exception $e) {
            info($e->getMessage());
        }

    }

    public function reset($code, $password)
    {
        if ($user = User::where("reset_password_code", "=", $code)->first()) {
            return $user;
        } else {
            return false;
        }
    }

    public function findUserByEmail($email)
    {
        if ($user = User::where('email', $email)->first()) {
            return $user->toArray();
        }

        return null;
    }
}