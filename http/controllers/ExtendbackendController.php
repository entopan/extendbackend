<?php namespace Entopancore\Extendbackend\Http\Controllers;

use Entopancore\Extendbackend\Http\Repositories\ExtendbackendRepositoryInterface;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Validator;
use ValidationException;

class ExtendbackendController extends Controller
{
    public $request;
    public $repository;


    public function __construct(ExtendbackendRepositoryInterface $repository, Request $request)
    {
        $this->request = $request;
        $this->repository = $repository;
    }

    public function redirect()
    {
        return redirect()->to("backend");
    }

    /**
     * @SWG\Post(
     *   path="/public/backend/login",
     *   tags={"Backend"},
     *   summary="Login with login and password",
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Body",
     *     required=true,
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="login", type="string", example="f.passanti"),
     *         @SWG\Property(property="password", type="string")
     *     )
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function login()
    {
        $data = $this->request->input();

        $rules = [
            'login' => 'required|between:2,255',
            'password' => 'required|between:4,255'
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            return getValidationResult($validation);
        }

        $user = $this->repository->login($data);

        if ($user['status'] == 200) {
            $this->repository->loginAccess($user["data"]['user']['id'], $this->request->ip());
            return getSuccessResult($user['data'], $user['message']);
        } elseif ($user['status'] == 400) {
            return getValidationResult();
        } elseif ($user['status'] == 404) {
            return getNotFoundResult($user["message"]);
        } elseif ($user['status'] == 500) {
            return getErrorResult($user["message"]);
        }
    }

    public function check()
    {
        return getSuccessResult();
    }

    /**
     * @SWG\Get(
     *   path="/public/backend/code-login",
     *   tags={"Backend"},
     *   summary="Code login",
     *   @SWG\Parameter(
     *     name="code",
     *     in="query",
     *     description="Code login",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function codeLogin()
    {
        $code = get("code");

        $user = \Backend\Models\User::where("code", "=", $code)->first();

        if ($user) {
            \BackendAuth::login($user);
            return redirect(\Backend::url("/"));
        } else {
            return redirect(route("backendLogout"));
        }
    }

    /**
     * @SWG\Get(
     *   path="/public/backend/logout",
     *   tags={"Backend"},
     *   summary="Logout",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=404, description="not found"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout()
    {
        \BackendAuth::logout();
        return redirect(\Backend::url("/"));
    }

    public function restore()
    {
        $data["email"] = $this->request["email"];

        $rules = [
            'email' => 'required|between:4,255|email|exists:backend_users'
        ];


        $validation = \Validator::make(["email" => trim($data["email"])], $rules, trans('entopancore.extendbackend::messages.v'));

        if ($validation->fails()) {
            return getValidationResult($validation);
        }

        if ($user = $this->repository->findUserByEmail($data["email"])) {
            if ($user["is_activated"]) {
            } else {
                if ($result = $this->userActivationCondition($user)) {
                    switch ($result["type"]) {
                        case "admin":
                            return getSuccessResult(null, $result["message"], ["type" => "admin"]);
                            break;
                        case "user":
                            return getSuccessResult(null, $result["message"], ["type" => "user"]);
                            break;
                    }
                }
            }
        } else {
            return getErrorResult();
        }
    }

    public function reset()
    {
        $data = $this->request->input();
        $rules = [
            'code' => 'required|exists:backend_users,reset_password_code',
            'password' => 'required|between:4,255|confirmed'
        ];
        $validation = \Validator::make($data, $rules, trans('entopancore.extendbackend::messages.v'));

        if ($validation->fails()) {
            return getValidationResult($validation);
        }

        if ($user = $this->repository->reset($data["code"], $data["password"])) {
            $url = implode("", [config("app.url_frontend"), config("entopancore.extendbackend::login_page")]);
            return getSuccessResult(null, trans('entopancore.extendbackend::lang.reset_password_success'), ["url" => $url]);
        }

        return getErrorResult(trans('entopancore.extendbackend::lang.reset_password.invalid_code'));

    }

}